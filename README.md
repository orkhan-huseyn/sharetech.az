# Sharetech.az Website

Run `yarn install` to install all development dependencies.

To run this application on production mode, run `yarn prod`. It will compile sass and javascript files and minify them, then will start `http-server` on port 80. Then go to browser and enter `http://localhost` on the url bar to see the website.

To run it on development mode, first run `yarn compile:js` to compile javascript files then run `yarn dev`. It will compile and watch sass files and concurrently run `http-server` on port 3000. Then go to browser and enter `http://localhost:3000` on the url bar to see the website.

## Pages

- `index.html` Home page
- `not-found.html` Just not found page
- `content.html` Single article page

## Performance and accessibility test

Lighthouse performance and accessibility test results on mobile:

![Lighthouse performance and  accessibility test results on  mobile](./lighthouse-mobile.png)

Lighthouse performance and accessibility test results on desktop:

![Lighthouse performance and  accessibility test results on  desktop](./lighthouse-desktop.png)
