const initCarousel = (carouselRoot) => {
  const carouselSlide = carouselRoot.querySelector('.carousel__slide');
  const carouselImages = carouselRoot.querySelectorAll('.carousel__slide img');
  const current = carouselRoot.querySelector('.carousel__actions__current');

  const prevBtn = carouselRoot.querySelector('.btn--prev');
  const nextBtn = carouselRoot.querySelector('.btn--next');

  let counter = 0;
  let size = carouselImages[0].clientWidth;
  const length = carouselImages.length;

  const updateCurrent = () => {
    current.innerHTML = `${counter + 1}/${length}`;
  };

  const resetCarousel = () => {
    size = carouselImages[0].clientWidth;
    counter = 0;
    carouselSlide.style.transform = `translateX(0px)`;
    updateCurrent();
  };

  const goToPrevious = () => {
    if (counter === 0) return;
    counter--;
    carouselSlide.style.transform = `translateX(${-size * counter}px)`;
    updateCurrent();
  };

  const goToNext = () => {
    if (counter === length - 1) return;
    counter++;
    carouselSlide.style.transform = `translateX(${-size * counter}px)`;
    updateCurrent();
  };

  const onKeyDown = (event) => {
    const focusedElement = document.activeElement;
    if (!carouselRoot.contains(focusedElement)) return;

    if (event.key === 'ArrowRight') goToNext();
    else if (event.key === 'ArrowLeft') goToPrevious();
  };

  updateCurrent();

  window.addEventListener('resize', resetCarousel);
  document.addEventListener('keydown', onKeyDown);
  prevBtn.addEventListener('click', goToPrevious);
  nextBtn.addEventListener('click', goToNext);
};
