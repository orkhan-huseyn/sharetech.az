(function () {
  const header = document.querySelector('.header');
  const toggleSearchButton = document.querySelector('#btnToggleSearchForm');
  const searchInput = document.querySelector('#searchInput');

  const toggleSearchForm = () => {
    header.classList.toggle('form--open');
    if (header.classList.contains('form--open')) searchInput.focus();
  };

  toggleSearchButton.addEventListener('click', toggleSearchForm);
})();
